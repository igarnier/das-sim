open Dagger
open Lmh_incremental_inference
module Int_set = Diet.Int
module I = Int_set.Interval

(* courtesy of Tezos stdlib *)
let shuffle ~rng_state a =
  let a = Array.copy a in
  let len = Array.length a in
  for i = len downto 2 do
    let m = RNG.int rng_state i in
    let n' = i - 1 in
    if m <> n' then (
      let tmp = a.(m) in
      a.(m) <- a.(n') ;
      a.(n') <- tmp)
  done ;
  a

let random_permutation array =
  Dist.dist0
    (fun rng_state -> shuffle ~rng_state array)
    (fun perm ->
      Log_space.unsafe_cast (Stats.Specfun.log_factorial (Array.length perm)))

type staker = int

type shard = int

let stakes : (staker * float) array =
  Array.mapi
    (fun i stake -> (i, stake))
    [| 9.35138330e+13;
       5.25637634e+13;
       4.60588234e+13;
       3.47177407e+13;
       3.01695052e+13;
       2.67333541e+13;
       1.95430687e+13;
       1.61429247e+13;
       1.48159805e+13;
       1.26159572e+13;
       1.16464298e+13;
       1.16134054e+13;
       1.13209483e+13;
       1.13160829e+13;
       1.12605521e+13;
       1.12423465e+13;
       1.10388401e+13;
       1.08000997e+13;
       9.40574860e+12;
       8.76640510e+12;
       7.58396800e+12;
       7.58332270e+12;
       7.58322681e+12;
       7.58202960e+12;
       7.58175636e+12;
       7.58033240e+12;
       7.57987304e+12;
       7.57901551e+12;
       7.32156801e+12;
       6.95069550e+12;
       6.81052476e+12;
       6.61991120e+12;
       5.20061832e+12;
       5.14156975e+12;
       5.10059621e+12;
       4.63464956e+12;
       4.52994856e+12;
       4.32582681e+12;
       3.85862473e+12;
       3.85860409e+12;
       3.70194451e+12;
       3.58398394e+12;
       3.24131987e+12;
       3.20175156e+12;
       3.08971200e+12;
       2.88854475e+12;
       2.61407889e+12;
       2.54756088e+12;
       2.53138876e+12;
       2.45340248e+12;
       2.43228641e+12;
       2.32157797e+12;
       2.23020828e+12;
       2.12272533e+12;
       2.11763540e+12;
       2.00275113e+12;
       1.85635735e+12;
       1.85560126e+12;
       1.67531864e+12;
       1.62936365e+12;
       1.58031644e+12;
       1.55440892e+12;
       1.49371721e+12;
       1.48253211e+12;
       1.31945577e+12;
       1.31122534e+12;
       1.29883546e+12;
       1.28022750e+12;
       1.24811936e+12;
       1.20902363e+12;
       1.14514602e+12;
       1.13591772e+12;
       1.01127737e+12;
       9.75777522e+11;
       9.30871580e+11;
       9.22044789e+11;
       9.09440286e+11;
       8.86192340e+11;
       8.52066955e+11;
       8.30101538e+11;
       8.13979011e+11;
       8.06929515e+11;
       7.77978115e+11;
       7.74226983e+11;
       7.29708123e+11;
       7.25228540e+11;
       7.23499781e+11;
       6.77497492e+11;
       6.48301197e+11;
       6.43579824e+11;
       5.79742066e+11;
       5.43370745e+11;
       5.39314241e+11;
       5.32549214e+11;
       5.17921256e+11;
       5.14089180e+11;
       5.12158510e+11;
       4.97810969e+11;
       4.94459989e+11;
       4.91730070e+11;
       4.85096663e+11;
       4.60423078e+11;
       4.51248512e+11;
       4.37703816e+11;
       4.33554732e+11;
       4.24427648e+11;
       4.09223266e+11;
       4.01330145e+11;
       3.86638547e+11;
       3.81606355e+11;
       3.79990739e+11;
       3.79712484e+11;
       3.71498597e+11;
       3.63837284e+11;
       3.63368500e+11;
       3.50194007e+11;
       3.46274206e+11;
       3.43813857e+11;
       3.42033373e+11;
       3.35024055e+11;
       3.27345870e+11;
       3.26355924e+11;
       3.24961726e+11;
       3.21765884e+11;
       3.21250744e+11;
       3.12827853e+11;
       2.98955519e+11;
       2.98307139e+11;
       2.93661203e+11;
       2.92955327e+11;
       2.90185882e+11;
       2.74400916e+11;
       2.65681578e+11;
       2.65469703e+11;
       2.63578788e+11;
       2.59176282e+11;
       2.48421167e+11;
       2.47922882e+11;
       2.40249046e+11;
       2.40026184e+11;
       2.22341331e+11;
       2.21379958e+11;
       2.08063668e+11;
       2.06910959e+11;
       2.06070273e+11;
       1.89832157e+11;
       1.85354812e+11;
       1.82738138e+11;
       1.77925909e+11;
       1.74604730e+11;
       1.73132240e+11;
       1.68865952e+11;
       1.66016485e+11;
       1.59996141e+11;
       1.58102911e+11;
       1.55879602e+11;
       1.55317288e+11;
       1.52914870e+11;
       1.48934394e+11;
       1.46814582e+11;
       1.46482540e+11;
       1.42098285e+11;
       1.39318129e+11;
       1.33794825e+11;
       1.33101702e+11;
       1.30358273e+11;
       1.28327933e+11;
       1.27134869e+11;
       1.24233712e+11;
       1.22191511e+11;
       1.21249854e+11;
       1.19051468e+11;
       1.18876048e+11;
       1.14814191e+11;
       1.14141013e+11;
       1.11947207e+11;
       1.06861121e+11;
       1.04169553e+11;
       1.04075837e+11;
       1.03462333e+11;
       9.96168527e+10;
       9.92539452e+10;
       9.89410663e+10;
       9.88409057e+10;
       9.87759121e+10;
       9.75927411e+10;
       9.24667189e+10;
       9.06028268e+10;
       9.04287286e+10;
       8.89957288e+10;
       8.36469305e+10;
       8.34141043e+10;
       8.22498193e+10;
       8.21732689e+10;
       8.13897807e+10;
       7.96737695e+10;
       7.94247560e+10;
       7.45568902e+10;
       7.32688883e+10;
       7.32339292e+10;
       7.27875358e+10;
       7.27252412e+10;
       7.20380420e+10;
       7.00491212e+10;
       6.99424259e+10;
       6.76214779e+10;
       6.66559219e+10;
       6.61067951e+10;
       6.59729246e+10;
       6.55777587e+10;
       6.51016742e+10;
       6.50036200e+10;
       6.46584752e+10;
       6.42089842e+10;
       6.42021485e+10;
       6.33706466e+10;
       6.29754061e+10;
       6.23157969e+10;
       6.16128188e+10;
       6.00458823e+10;
       5.88977203e+10;
       5.85858425e+10;
       5.81845022e+10;
       5.79831724e+10;
       5.49491659e+10;
       5.44104959e+10;
       5.43017444e+10;
       5.39161129e+10;
       5.33454860e+10;
       4.98975160e+10;
       4.93286682e+10;
       4.88306674e+10;
       4.80747887e+10;
       4.65004303e+10;
       4.53998888e+10;
       4.52869389e+10;
       4.52491552e+10;
       4.47873559e+10;
       4.42835805e+10;
       4.41280222e+10;
       4.37285024e+10;
       4.37085946e+10;
       4.35182600e+10;
       4.34981196e+10;
       4.32506630e+10;
       4.32388059e+10;
       4.32176727e+10;
       4.30846618e+10;
       4.30602101e+10;
       4.28506809e+10;
       4.25241232e+10;
       4.24075200e+10;
       4.11545335e+10;
       4.08625167e+10;
       4.07244859e+10;
       4.04841371e+10;
       3.98551501e+10;
       3.97899694e+10;
       3.92940348e+10;
       3.77922936e+10;
       3.61110400e+10;
       3.61075246e+10;
       3.12697410e+10;
       3.11399402e+10;
       3.08623308e+10;
       3.02786965e+10;
       3.01428841e+10;
       2.90951208e+10;
       2.88763705e+10;
       2.86473282e+10;
       2.85008446e+10;
       2.75746206e+10;
       2.67015875e+10;
       2.66693594e+10;
       2.53881127e+10;
       2.50672576e+10;
       2.47926429e+10;
       2.47528272e+10;
       2.46734522e+10;
       2.45295430e+10;
       2.45295265e+10;
       2.44316778e+10;
       2.41105269e+10;
       2.40647145e+10;
       2.40437393e+10;
       2.38865204e+10;
       2.35979455e+10;
       2.35383881e+10;
       2.05521966e+10;
       2.01538430e+10;
       1.89975600e+10;
       1.89111061e+10;
       1.81045041e+10;
       1.80962152e+10;
       1.78772929e+10;
       1.78540626e+10;
       1.77050885e+10;
       1.76300731e+10;
       1.72071694e+10;
       1.71671568e+10;
       1.68929412e+10;
       1.68064893e+10;
       1.68052439e+10;
       1.67504953e+10;
       1.67265672e+10;
       1.64559518e+10;
       1.64252048e+10;
       1.61897607e+10;
       1.61448640e+10;
       1.60469980e+10;
       1.60124241e+10;
       1.53770312e+10;
       1.46516398e+10;
       1.43856601e+10;
       1.40406213e+10;
       1.39197995e+10;
       1.34946638e+10;
       1.33946321e+10;
       1.32718141e+10;
       1.31029237e+10;
       1.25359799e+10;
       1.23227197e+10;
       1.21374962e+10;
       1.21197605e+10;
       1.21085582e+10;
       1.20693948e+10;
       1.18860738e+10;
       1.15696180e+10;
       1.08038944e+10;
       1.06575132e+10;
       1.05296503e+10;
       1.04580002e+10;
       1.04115017e+10;
       9.63823078e+09;
       9.52034448e+09;
       9.22348311e+09;
       9.21658783e+09;
       9.19949332e+09;
       9.03935286e+09;
       8.94419832e+09;
       8.80474546e+09;
       8.80181171e+09;
       8.61851694e+09;
       8.57134550e+09;
       8.56633713e+09;
       8.55642608e+09;
       8.43482083e+09;
       8.42456627e+09;
       8.36575945e+09;
       8.35150244e+09;
       8.31547381e+09;
       8.28128934e+09;
       8.27119544e+09;
       8.22870652e+09;
       8.20638757e+09;
       8.20622146e+09;
       8.19767964e+09;
       8.16691101e+09;
       8.16590057e+09;
       8.15252771e+09;
       8.12856811e+09;
       8.11058044e+09;
       8.08349879e+09;
       8.07690348e+09;
       8.06066717e+09;
       8.05819681e+09;
       8.05150071e+09;
       8.04663894e+09;
       8.04169850e+09;
       8.03557392e+09;
       7.99999938e+09;
       7.05884504e+09;
       6.99700374e+09;
       6.44846775e+09;
       6.29661709e+09;
       6.09785661e+09;
       6.01046915e+09;
       6.01030760e+09;
       6.00909668e+09;
       6.00199938e+09;
       5.45404645e+09;
       2.51151010e+09;
       1.80311448e+09;
       1.37799901e+09;
       8.99798915e+08;
       7.77627725e+08;
       8.67035100e+07;
       1.24459720e+07;
       1.24195920e+07;
       9.99937700e+06;
       4.01573800e+06;
       3.83365000e+06;
       1.34673600e+06;
       1.28225500e+06;
       1.23986400e+06;
       9.98972000e+05;
       9.95346000e+05;
       9.92554000e+05;
       9.27133000e+05;
       8.92989000e+05;
       4.99271000e+05;
       4.99271000e+05;
       4.45660000e+05;
       3.12132000e+05;
       1.11831000e+05;
       9.81650000e+04;
       3.43130000e+04;
       3.89000000e+03;
       4.00000000e+00;
       2.00000000e+00;
       1.00000000e+00;
       0.00000000e+00
    |]

let stake_dist : staker Dist.t = Stats_dist.categorical Stats.int_table stakes

let nb_bakers = Array.length stakes

let nb_peers = nb_bakers

(*
We observe the impact of erasure code redundancy and fraction of the shares to distribute to a peer in terms of downloaded data & storage.

In order to retrieve data with 50% data availability, we use an erasure code.
Given a vector of k elements in the prime field from BLS $\mathbb{F}_r$, we add some redundancy and obtain a vector of length $n$.
The erasure code has the property that any subset of $k$ elements of the encoded data is enough to recover the original data.

Such $[n=2k,k]$ erasure code guarantees that contacting 50% of the network is sufficient to recover the original data, assuming an
equal distribution of the shares between the peers. However, that is not the case in Mainnet, where each peer receives a fraction of
the shares proportional to its stake. In the latter case, we're guaranted to recover the data by contacting any subset of peers totalling
50% of the shares.

Hence we may want to try different strategies for collecting shares: either contact peers at random, or peers with the highest staking
balance (best case). We note that the best case in the simulations below correspond approximately to the latter situation (contacting
peers with high staking balance).

In the simulation, we contact peers at random until we collect enough data shares to reconstruct the data.
We then compute the
- mean (average)
- min (best case)
- max (worst case)

of peers to contact for random subsets of queried nodes (downloaded data).

The min gives our data availability/security level (fraction of the network that must be online to decode data).
We then test if other code parameters allow to reduce the overall storage for a fixed data availability.

Here are the parameters of the simulations:
*)

let nb_shards = 2048

(* The prime field we're encoding the data to has order ~ 2^256; hence an element can be
   stored on 32 bytes.

   In this model, we consider storing a slot of 1MB of data. The number of field elements
   required to encode this data (assuming no redundancy) is 10^6 / 32 = 31250 ~ 32768 = 2 ^ 15 *)
let k = 1 lsl 15 (* 2 ^ 15 *)

(* Each shard is associated to a finite subset of elements encoding part of the data.
   Each shard is stored to
   [replication_constant] peers sampled at random according to the stakes as probability
   law for picking one peer. *)
let stakeholder_allocation (replication_constant : int)
    (sharded_elements : Int_set.t array) =
  let shard_bearers_dist = Dist.iid replication_constant stake_dist in
  map_array (Array.init nb_shards (fun _ -> sample shard_bearers_dist))
  @@ fun per_shard_alloc ->
  let elements_held_by_stakers = Array.make nb_bakers Int_set.empty in
  Array.iteri
    (fun shard alloc ->
      let elements_of_shard = sharded_elements.(shard) in
      Array.iter
        (fun staker ->
          elements_held_by_stakers.(staker) <-
            Int_set.union elements_of_shard elements_held_by_stakers.(staker))
        alloc)
    per_shard_alloc ;
  Array.map Int_set.cardinal elements_held_by_stakers

(* The following function retrieves data from a subset of peers until a fraction of k/n = 1/erasure_stretch_factor of the shares is collected *)
let download_shards shares shuffle =
  let rec loop i nb_contacted_peers collected =
    if i = Array.length shuffle || collected >= k then
      (nb_contacted_peers, collected)
    else loop (i + 1) (nb_contacted_peers + 1) (collected + shares.(shuffle.(i)))
  in
  let ((_, collected) as res) = loop 0 0 0 in
  if collected < k then None else Some res

let data_availability_and_storage_overhead erasure_stretch_factor
    replication_constant =
  (* number of field elements for the erasure-encoded data *)
  let n = erasure_stretch_factor * k in

  (* allocation of field elements to each shard *)
  assert (n mod nb_shards = 0) ;

  let open Infix in
  let+ elements_per_staker =
    let elements_per_shard = n / nb_shards in
    let sharded_elements =
      Array.init nb_shards (fun shard_index ->
          let elements_of_shard =
            I.make
              (shard_index * elements_per_shard)
              (((shard_index + 1) * elements_per_shard) - 1)
          in
          Int_set.add elements_of_shard Int_set.empty)
    in
    stakeholder_allocation replication_constant sharded_elements
  and+ staker_shuffle =
    (* integrating over all permutations of stake is intractable ... *)
    sample @@ random_permutation (Array.map fst stakes)
  in
  download_shards elements_per_staker staker_shuffle

let display_results ~erasure_stretch_factor ~replication_constant =
  let rng_state = RNG.make_self_init () in
  let nsamples = 10000 in
  let samples =
    stream_samples
      (data_availability_and_storage_overhead
         erasure_stretch_factor
         replication_constant)
      rng_state
    |> Containers.Seq.drop 1000
    |> Containers.Seq.take nsamples
  in
  let results = Array.of_seq samples in
  let total_none =
    Array.fold_left
      (fun acc opt -> if opt = None then acc + 1 else acc)
      0
      results
  in
  let fraction_none = float total_none /. float nsamples in
  let _fraction_some = 1. -. fraction_none in
  let conditioned_on_success = CCArray.filter_map Fun.id results in
  let contacted_when_success = Array.map fst conditioned_on_success in
  let fraction_of_peers x = float x /. float nb_peers in
  let min_contacted =
    let min = Array.fold_left Int.min Int.max_int contacted_when_success in
    Format.printf "min: %d@." min ;
    fraction_of_peers min
  in
  let max_contacted =
    let max = Array.fold_left Int.max Int.min_int contacted_when_success in
    Format.printf "max: %d@." max ;
    fraction_of_peers max
  in
  let avg_contacted =
    float (Array.fold_left ( + ) 0 contacted_when_success)
    /. float (Array.length contacted_when_success)
    /. float nb_peers
  in
  let open Format in
  printf
    "stretch factor = %d, replication constant = %d@."
    erasure_stretch_factor
    replication_constant ;
  printf
    "probability of failure (over %d samples) = %f@."
    nsamples
    fraction_none ;
  printf "conditioned on success:@." ;
  printf " min contacted = %f@." min_contacted ;
  printf " max contacted = %f@." max_contacted ;
  printf " avg contacted = %f@." avg_contacted

let () = display_results ~erasure_stretch_factor:16 ~replication_constant:1
