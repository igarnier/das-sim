open Dagger
open Lmh_incremental_inference

(* We could make this part of the optimization *)
let alpha = 16

let allocation shards =
  let open Infix in
  let s = float shards in
  let parameters =
    let+ f = sample @@ Stats_dist.float 1.0
    and+ t = sample @@ Stats_dist.float 1.0 in
    (f, t)
  in
  map_score parameters (fun (f, t) ->
      let st = int_of_float (s *. t) in
      let c = max 0 ((shards / alpha) - 1) in
      let lower_tail = Gsl.Cdf.binomial_P ~k:c ~p:f ~n:st in
      if lower_tail < 1e-9 then 1.0 else 0.0)

let rng_state = RNG.make_self_init ()

let nsamples = 10_000

let posterior =
  stream_samples (allocation 2048) rng_state
  |> Seq.drop 5000 |> Seq.take nsamples

let () =
  let open Plot in
  let plot =
    plot2
      ~xaxis:"f"
      ~yaxis:"t"
      [Scatter.points_2d ~points:(posterior |> Seq.map tup_r2 |> Data.of_seq) ()]
  in
  run ~target:(qt ()) exec plot
