# -*- coding: utf-8 -*-
"""ErasureWithReplication.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/14mO8OOQB3I27pMUXJtW-S4XN6IADxDL-

## Quick simulations on erasure code redundancy and fraction of the shares to distribute to a peer

We try to assess empirically the constants of the DAS specification for the erasure code and the amount of shares to distribute to the peers with regard to downloaded data and storage.

We collect all active bakers from Mainnet for use in the simulations below. Coinbase has the highest fraction of all the stakes combined (~14%).
"""

import numpy as np
import random
from math import *
import requests

# Retrieve all active bakers
api_url = "https://api.tzkt.io/v1/delegates?active&sort.desc=stakingBalance&limit=500"
response = requests.get(api_url).json()

stakes = []
for item in response:
    stakes.append(float(item['stakingBalance']))

sum_stakes = sum(stakes)
stakes = np.array(stakes)

stakes /= sum_stakes
# the stakes are sorted in increasing order

# stakes = np.array([1/len(stakes) for i in range(len(stakes))]) uniform distribution of the stakes
# for a uniform distribution of the stakes, erasure coding performs well for lower code rates k/n ~ 1/2

nb_bakers = len(stakes)
nb_peers = nb_bakers

print("found", len(stakes), "active bakers")

"""We observe the impact of erasure code redundancy and fraction of the shares to distribute to a peer in terms of downloaded data & storage.

In order to retrieve data with 50% data availability, we use an erasure code. Given a vector of k elements in the prime field from BLS $\mathbb{F}_r$, we add some redundancy and obtain a vector of length $n$. The erasure code has the property that any subset of $k$ elements of the encoded data is enough to recover the original data.

Such $[n=2k,k]$ erasure code guarantees that contacting 50% of the network is sufficient to recover the original data, assuming an equal distribution of the shares between the peers. However, that is not the case in Mainnet, where each peer receives a fraction of the shares proportional to its stake. In the latter case, we're guaranted to recover the data by contacting any subset of peers totalling 50% of the shares.

Hence we may want to try different strategies for collecting shares: either contact peers at random, or peers with the highest staking balance (best case). We note that the best case in the simulations below correspond approximately to the latter situation (contacting peers with high staking balance).

In the simulation, we contact peers at random until we collect enough data shares to reconstruct the data. We then compute the mean (average), min (best case), max (worst case) of peers to contact for random subsets of queried nodes (downloaded data). The min gives our data availability/security level (fraction of the network that must be online to decode data). We then test if other code parameters allow to reduce the overall storage for a fixed data availability.

Here are the parameters of the simulations:
"""

# The erasure-encoded data splits evenly into [nb_shards] shards. We may want to alter this value.
# If the erasure stretch factor is twice the proposed factor of 2, then the shard length doubles.
# Question: do we want to keep the number of shards fixed when increasing the erasure stretch factor?
nb_shards = 2048


# 1 slot of 1 MB encoded (not erasure-encoded!) in a vector of elements in the considered prime field: 10^6 bytes / 32 bytes is slightly lower than 2^15
# indeed, the order of the field is around 2^256, so an element of the prime field fits into 256 bits/8 = 32 bytes
# is the length of this vector
k = 2**15

"""Each shard is stored to [replication_constant] peers sampled at random according to the stakes as probability law for picking one peer."""

def distribute_shards(n, shard_size, shards_indices, sharded_data_indices, replication_constant):
    storage = 0
    shares = [[] for i in range(nb_peers)]

    # The following commented code distributes random (non-disjoint) subsets of shards to the participating peers.
    # uploading data to the peers
    #shares = []
    #for i in range(nb_peers):
    #    tmp = []
    #    # selecting a **subset** of the shards as there's no point in picking the same share twice
    #    # indeed, we're trying to recover the data from offline peers, not disk erasures
    #    peer_storage = min(ceil(replication_constant * stakes[i] * n / shard_size), len(shards_indices))
    #    storage += peer_storage * shard_size
    #    idx = random.sample(list(shards_indices), peer_storage)
    #    for j in range(len(idx)):
    #        tmp.append(sharded_data_indices[idx[j]])
    #    shares.append(tmp)

    # the following distribution guarantees that each share is stored by at least one node
    peers = np.arange(0, nb_peers)
    for i in range(nb_shards):
        current_shard = sharded_data_indices[i]
        for t in range(replication_constant):
            rd = np.random.choice(peers, p=stakes)
            storage += shard_size
            shares[rd].append(current_shard)
    return shares, storage

"""The following function retrieves data from a subset of peers until a fraction of k/n = 1/erasure_stretch_factor of the shares is collected """

def download_shards(shares, nb_biggest_bakers_offline):
    idx  = [i for i in range(nb_peers)]
    random.shuffle(idx)
    collected_shares = []
    nb_contacted_peers = 0
    for i in range(len(idx)):
        if i >= len(stakes) - nb_biggest_bakers_offline:
                continue

        nb_contacted_peers += 1

        collected_shares.append(flatten(shares[idx[i]]))

        if len(flatten(collected_shares)) >= k:
            break

    if len(flatten(collected_shares)) < k:
        return None
    return collected_shares, nb_contacted_peers

def flatten(t):
    return list(set([item for sublist in t for item in sublist]))

def data_availability_and_storage_overhead(erasure_stretch_factor, replication_constant, nb_biggest_bakers_offline=0, nb_trials=100):
    # parameters of the erasure code
    k = 2**15
    n = erasure_stretch_factor * k

    # indices of the encoded data
    data_indices = np.array([i for i in range(n)])
    sharded_data_indices = np.array_split(data_indices, nb_shards)
    shard_size = len(sharded_data_indices[0])
    shards_indices = [i for i in range(nb_shards)]

    contacted = np.arange(0, nb_trials)
    storages = np.arange(0, nb_trials)

    for t in range(nb_trials):
        shares, storage = distribute_shards(n, shard_size, shards_indices, sharded_data_indices, replication_constant)
        storages[t] = storage

        res = download_shards(shares, nb_biggest_bakers_offline)
        if res is None:
            return None
        collected_shares, nb_contacted_peers = res
        contacted[t] = nb_contacted_    # we return the minimum, average and maximum fraction of queried nodes in order to decode
    # the maximum fraction of queried nodes gives our data availability (worst case)
    return np.min(contacted)/nb_peers, np.mean(contacted)/nb_peers, np.max(contacted)/nb_peers, np.mean(storage)/k

def display_results(erasure_stretch_factor, replication_constant, nb_biggest_bakers_offline=0, nb_trials=100):
    print("with n = {}k".format(erasure_stretch_factor))
    print("replication factor of {} * stake_percentage * nb_shares:".format(replication_constant))
    res = data_availability_and_storage_overhead(erasure_stretch_factor, replication_constant, nb_biggest_bakers_offline, nb_trials)
    if res is None:
        print("couldn't decode")
    else:
        minimum, avg, maximum, storage_overhead = res
        print("min of queried nodes {:.2f}%".format(minimum))
        print("avg of queried nodes {:.2f}%".format(avg))
        print("max of queried nodes {:.2f}%".format(maximum))
        print("avg storage overhead (total storage/k) : {:.2f}x".format(storage_overhead))

"""We consider an erasure code redundancy factor of 2, and increasing "replication" factor:"""

for erasure_fac in range(1, 4):
    for replication_fac in range(1, 5):
        display_results(2**erasure_fac, 2**replication_fac)

display_results(16, 1)

"""Which number of the bakers concentrating a big fraction of the shares offline can be tolerated?"""

display_results(16, 1, nb_biggest_bakers_offline=100)

"""Increasing the number of bakers having the biggest stakes that are offline increases the number of queried nodes."""

display_results(2, 16)

display_results(32, 1, nb_biggest_bakers_offline=350)

display_results(8, 1, nb_biggest_bakers_offline=0)

display_results(2, 1, nb_biggest_bakers_offline=300)

display_results(16, 2, nb_biggest_bakers_offline=300)

"""As the distribution of the shares is not uniform, increasing the erasure code factor allows to decode with a higher fraction of the biggest bakers offline. We find than an erasure code with parameter n=16k and a replication constant of 1 allows to recover data with ~70% of the biggest bakers offline.

Here are the timings for encoding and decoding (OCaml implementation) with these parameters as of now:

k = 2^15 ; n = 16 * k

RS encoding : 0.368908 s.

RS decoding : 3.470843 s.
"""
